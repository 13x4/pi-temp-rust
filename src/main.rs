// for proper exit
use std::process;
// for temperature file openinng
use std::env;
use std::fs;
// for delay
use std::thread::sleep;
use std::time;
// for timestamps
use chrono;
// for clearscreen;
use clearscreen;

fn main() {
    let mut delay = 2;
    let mut time_stamp: bool = false;
    let mut clear_screen: bool = false;
    // path to file, where temperature is stored
    let path_to_file = "/sys/class/thermal/thermal_zone0/temp";

    // reading/checking arguments
    if env::args().len() > 0 {
        for (index, argument) in env::args().enumerate() {
            match argument.as_str() {
                "-t" => time_stamp = true,
                "-r" => clear_screen = true,
                "-d" => {
                    delay = env::args()
                        .nth(index + 1)
                        .unwrap()
                        .parse()
                        .expect("\n=== Error reading number after -d\n")
                }
                "-h" => {
                    print_help();
                    process::exit(1);
                }
                &_ => (),
            }
        }
    }

    let delay = time::Duration::from_millis(delay * 1000);

    //main routine
    loop {
        let now = chrono::Local::now().format("%Y-%m-%d %H:%M:%S").to_string();
        let temperature: u32 = fs::read_to_string(path_to_file)
            .expect("\n=== Error reading file.\n")
            .trim()
            .parse()
            .expect("\n=== Error converting to number.\n");
        if time_stamp == true {
            print!("{}: ", now);
        }
        println!("Temp = {} C'", temperature / 1000);
        sleep(delay);
        if clear_screen == true {
            clearscreen::clear().expect("\n=== Failed to clear screen.");
        }
    }
}

fn print_help() {
    println!(
        "
        Usage: ./[program name] [options]
        Monitor CPU temperature.
        
        Options:
        -d [NUM]    delay in second between measures
        -t          print timestamp before temperature.
        -r          use monitor mode instead of history/list mode.
        -h,         show this help.
        
        Example:
        pi-temp -t -r -d 3
        
        Author: Aleksejs Abrosimovs
    "
    )
}
